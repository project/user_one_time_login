<?php

/**
 * @file
 * User One Time Login module install.
 */

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Implements hook_install().
 */
function user_one_time_login_install() {
  // Create the field storage.
  $field_storage_definition = [
    'field_name' => 'no_login',
    'entity_type' => 'user',
    'type' => 'boolean',
    'label' => t('Block login form'),
    'description' => t('If checked, the user will not be able to login with their username and password. Need a unique access link to log in.'),
    'cardinality' => BaseFieldDefinition::CARDINALITY_UNLIMITED,
    'translatable' => FALSE,
    'settings' => [],
  ];

  $field_storage = FieldStorageConfig::create($field_storage_definition);
  $field_storage->save();

  // Create the field.
  $field_definition = [
    'field_storage' => $field_storage,
    'bundle' => 'user',
    'label' => t('Block login form'),
    'description' => t('If checked, the user will not be able to login with their username and password. Need a unique access link to log in.'),
    'default_value' => [],
  ];

  $field = FieldConfig::create($field_definition);
  $field->save();

  // Get the form display for the user entity.
  $form_display = \Drupal::service('entity_display.repository')->getFormDisplay('user', 'user');

  // Add the 'no_login' field to the form display.
  $form_display->setComponent('no_login', [
    'type' => 'boolean_checkbox',
    'settings' => [
      'display_label' => TRUE,
    ],
    'weight' => 10,
  ]);
  // Save the form display.
  $form_display->save();
}

/**
 * Implements hook_uninstall().
 */
function user_one_time_login_uninstall() {
  if ($field = FieldConfig::loadByName('user', 'user', 'no_login')) {
    $field->delete();
  }
  if ($field_storage = FieldStorageConfig::loadByName('user', 'no_login')) {
    $field_storage->delete();
  }
}
