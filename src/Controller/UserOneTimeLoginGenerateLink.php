<?php

namespace Drupal\user_one_time_login\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * Generate one time login link for Drupal users.
 *
 * @package Drupal\user_one_time_login\Controller
 */
class UserOneTimeLoginGenerateLink extends ControllerBase {

  /**
   * Generate one time login link.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to front page.
   */
  public function oneTimeLoginLink(UserInterface $user) {
    $timestamp = REQUEST_TIME;
    $token = user_pass_rehash($user, $timestamp);

    $url = Url::fromRoute('user.reset.login', [
      'uid' => $user->id(),
      'timestamp' => $timestamp,
      'hash' => $token,
    ])->setAbsolute();

    $this->messenger()->addMessage($this->t('The single-use login link for the user @username is: @url', [
      '@username' => $user->getDisplayName(),
      '@url' => $url->toString(),
    ]));

    return $this->redirect('<front>');
  }

}
