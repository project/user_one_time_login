<?php

namespace Drupal\user_one_time_login\Plugin\Action;

use Drupal\views_bulk_operations\Action\ViewsBulkOperationsActionBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Generate unique access link for users.
 *
 * @Action(
 *   id = "user_one_time_login_generate_link_action",
 *   label = @Translation("Generate unique access link"),
 *   type = "user"
 * )
 */
class GenerateLinkAction extends ViewsBulkOperationsActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $controller = \Drupal::classResolver()->getInstanceFromDefinition('Drupal\user_one_time_login\Controller\UserOneTimeLoginGenerateLink');
    $controller->oneTimeLoginLink($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $access = $object->access('update', $account, TRUE);
    return $return_as_object ? $access : $access->isAllowed();
  }

}
