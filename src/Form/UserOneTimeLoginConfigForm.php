<?php

namespace Drupal\user_one_time_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Create the configuration form for the module.
 *
 * Settings are:
 *   - Blocked roles: roles that will be blocked from the login form.
 */
class UserOneTimeLoginConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['user_one_time_login.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_one_time_login_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('user_one_time_login.settings');

    $roles = user_role_names(TRUE);
    $form['blocked_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Blocked roles'),
      '#options' => $roles,
      '#default_value' => $config->get('blocked_roles') ?: [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('user_one_time_login.settings')
      ->set('blocked_roles', array_filter($form_state->getValue('blocked_roles')))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
