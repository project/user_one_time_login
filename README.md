# User One Time Login Module

This Drupal module allows you to lock the login form per role or individual user to force the use of a single sign-on link. It also allows you to generate unique login links for users. These links can be used to log into the site without needing a username and password.

## Usage

To block the login form for a role, go to the module's configuration page and select the role in the "Blocked roles" field. To block an individual user, edit the user and check the "Block login form" checkbox.

To generate a unique login link for a user, go to the user's list page (`/admin/people`) and click on the "Unique access link" operaration for the desired user. The link will be displayed on the message section.

To log into the site using a unique login link, go to the link and you will be automatically logged in. The link can only be used once.

## Installation

1. Download the module and place it in your Drupal modules directory (usually `/modules/` in the root of your Drupal installation), add it from the "Add new module" page (`/admin/modules/install`) or by using Composer.
  ```bash
    composer require "drupal/user_one_time_login"
  ```
2. Go to the modules administration page (`/admin/modules`) and enable the "User One Time Login" module.
3. Configure the module on the configuration page (`/admin/config/development/user_one_time_login`).

## Support

If you have any questions or issues with the module, please visit the module's homepage http://drupal.org/project/user_one_time_login.
